import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;


public class ImageGalleryDisplay extends HttpServlet {


    public ImageGalleryDisplay() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ServletContext servletContext = getServletContext();
        String fullPhotoPath = servletContext.getRealPath("/Photos");

        File dir = new File(fullPhotoPath);

        File[] listOfFiles = dir.listFiles();

        PrintWriter out = response.getWriter();


        if (dir.listFiles() == null) {
            out.println("<html>\n<head><title>Image Gallery Viewer</title>");
            out.println("</head>\n<body>");
            out.println("<h1>There are no image in the image gallery. </h1>");
            out.println("</body></html>");

        } else {

            out.println("<html>\n<head><title>Image Gallery Viewer</title>");
            out.println("</head>\n<body>");
            out.println("<h1>All image</h1>");


            for (int i = 0; i < listOfFiles.length; i++) {
                if (!(listOfFiles[i].getName().endsWith("_thumbnail.png"))) {
                    String name = listOfFiles[i].getName().replace(".jpg", "");
                    out.println("<p>Thumbnail of " + name);
                    out.println("Full Size: " + listOfFiles[i].length() + "</p>");
                    out.println("<a href=\"Photos/" + name + ".jpg\">");
                    out.println("<img src=\"Photos/" + name + "_thumbnail.png\"/></a>");
                    out.println("<hr>");
                }
                /* if ((listOfFiles[i].getName().endsWith("_thumbnail.png"))) {
                    String name = listOfFiles[i].getName().replace("_thumbnail.png", "");

                    out.println("<p>Thumbnail of " + name);
                    out.println("Size: " + listOfFiles[i].length() + "</p>");


                    out.println("<a href=\"Photos/" + name + ".jpg\">");
                    out.println("<img src=\"Photos/" + listOfFiles[i].getName() + "\"/></a>");
                }*/
            }


            out.println("</body></html>");


        }
    }
}
